#include "massfitter.h"

#include <ActiveQt/QAxFactory>


#include <string.h>
#include <math.h>
#include <windows.h>

#include <QFile.h>
#include <QTextStream.h>
#include <QFileDialog.h>
#include <QList>
#include <QString.h>
#include <QStandardItemModel>

#include "Chem.h"
#include "ElTableModel.h"
#include "IzTreeModel.h"

QList<NElement> g_ElList;
QList<NIzotop> g_IzList;
QList<NBrutoFormula> g_BrutoList;
bool g_bSkipIzotopes = false;

/*
int MassFitter::split(NBrutoFormula& BF, QList<NIzotop>::iterator itIz, float MassMin, float MassMax)//recoursive proc
{
	//MassMin, MassMax - ������� �� �����

	
	if(itIz==g_IzList.end()) {
		return 1;
	}
	QString sIzName = itIz->m_sName;
	
	//������������ ����� ������ ������� ��������, ����������� � ������� �����
	int IndMax=(int)floor(MassMax/itIz->m_fMass);
	int nMaxC =  itIz->m_pEl->m_nMaxCount;//MaxC[CurNum];
	if(nMaxC<IndMax) IndMax=  nMaxC;
	if (g_bSkipIzotopes && itIz->m_nType != 0){
		IndMax = 0;
	}
	float fMass=0.0;
//	char sNewBF[100];
	
	for(int i=0;i<=IndMax;i++){
		double fAddMass=i*itIz->m_fMass;// M[CurNum];
		double fIonMass = fAddMass - NElement::s_fElMass;
		NBrutoFormula NewBF(BF);
		if(fIonMass <= MassMax){	
			if(i>0){//����������� �������
				NewBF.AddItem(NBFItem(&(*itIz),i));
			}

			if( fIonMass > MassMin){//���������� ������-�������
				g_BrutoList.push_back(NewBF);				
				return 0;
			}

			//---------------------------------------------------------------------------------------------------------------
			split(NewBF,itIz+1, MassMin-fAddMass,MassMax-fAddMass);
		}
	}
	return 0;
}
*/
int MassFitter::split(const NBrutoFormula* pBaseBF, int nFromIzNum, double MassMin, double MassMax)
{
	//MassMin, MassMax - ������� �� �����

	int nIzSize = g_IzList.size();
	if(nFromIzNum==nIzSize) {
			return 1;
	}
	for(int nIzNum = nFromIzNum; nIzNum<nIzSize;nIzNum++){
		if (nFromIzNum == 0){
			if (1000 * nIzNum / nIzSize % 10 == 0){
				ui.progressBar->setValue(100*nIzNum/nIzSize);
			}
		}

		const NIzotop* pIzotope = &g_IzList[nIzNum];
		QString sIzName = pIzotope->m_sName;
		
		//������������ ����� ������ ������� ��������, ����������� � ������� �����
		int IndMax=(int)floor(MassMax/pIzotope->m_fMass);
		int nMaxC =  pIzotope->m_pEl->m_nMaxCount;//MaxC[CurNum];
		if(nMaxC<IndMax) IndMax=  nMaxC;
		if (g_bSkipIzotopes && pIzotope->m_nType != 0){
			IndMax = 0;
		}
		
		double fBaseMass = pBaseBF->m_fMass;
		for(int i=1;i<=IndMax;i++){
			double fAddMass=i*pIzotope->m_fMass;// M[CurNum];
			double fNewMass = fBaseMass + fAddMass;
			if (fNewMass < MassMax){
				NBrutoFormula NewBF(*pBaseBF);
				NewBF.AddItem(NBFItem(pIzotope,i));
				if (fNewMass < MassMin){
					split(&NewBF,nIzNum+1, MassMin,MassMax);
				}else{
					g_BrutoList.push_back(NewBF);
				}
			}
		}
	}
	if (nFromIzNum == 0){
		ui.progressBar->setValue(100);
	}
	return 0;
}

QList<NBrutoFormula> g_MainBF;
QList<NBrutoFormula> g_RareBF;
int g_nBranches = 0;


NElTableModel g_ElModel(&g_ElList);
NIzTreeModel g_IzModel(&g_MainBF,&g_RareBF);
QItemSelectionModel g_SelModel(&g_IzModel);
//QStandardItemModel g_stdmodel(2,4);

MassFitter::MassFitter(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.OutTextEdit->setReadOnly(true);
	
	//QFontMetrics fm( ui.tableView->font() );
	int nSignW = 16*4;//fm.width("Sign");
	int nMaxNW = 16*4;//fm.width("maxN");
//	ui.tableView->setMaximumWidth(nSignW + nMaxNW + 5);
	ui.tableView->setModel(&g_ElModel);
	ui.tableView->setColumnWidth(0,nSignW);
	ui.tableView->setColumnWidth(1,nMaxNW);
	ui.tableView->setColumnWidth(2,2*nMaxNW);

	ui.treeView->setModel(&g_IzModel);
	ui.treeView->setSelectionModel(&g_SelModel);
//	connect(&g_SelModel,SIGNAL(selectionChanged ( const QItemSelection & , const QItemSelection &  ) ),this, SLOT(selTreeChanged ( const QItemSelection & , const QItemSelection &  )));
	connect(&g_SelModel,SIGNAL(currentChanged ( const QModelIndex &, const QModelIndex &  )  ),this, SLOT(TreeCurrentChanged ( const QModelIndex & , const QModelIndex & ) ));
	
	LPTSTR  szProcPath = new TCHAR[_MAX_PATH];
	LPTSTR  drive = new TCHAR[ _MAX_DRIVE];
	LPTSTR  dir = new TCHAR[_MAX_DIR];
	LPTSTR  fname = new TCHAR[_MAX_FNAME];
	LPTSTR  ext = new TCHAR[_MAX_EXT ];
	::GetModuleFileName(NULL, szProcPath, _MAX_PATH);
	_wsplitpath( szProcPath, drive,dir,fname,ext );

//	LPTSTR  chemfile = new TCHAR[_MAX_PATH ];
//	wsprintf(chemfile, L"%s%s%s",drive,dir,L"Chem.txt.chmr");
//	QString sChemName = QString::fromStdWString ( std::wstring(chemfile) );
//	openChemFile(sChemName);

	sDefDir =  QString::fromStdWString ( std::wstring(drive) ) ;
	sDefDir +=  QString::fromStdWString ( std::wstring(dir) );

	QString sChemName = sDefDir + "Chem.txt.chmr"; 
	openChemFile(sChemName);

	QString sPreSetFName = sDefDir + "organic.txt.pch";
	openPresetFile(sPreSetFName);

//	delete[] chemfile;
	delete[] szProcPath;
	delete[] drive;
	delete[] dir;
	delete[] fname;
	delete[] ext;
 

}



void MassFitter::on_pbtnOpen_clicked()
{
	QString sFileName = QFileDialog::getOpenFileName(0,"Open mass data file","","*.txt");
	openDataFile(sFileName);
}

//const char[2][20] g_szGrNames = {"Main Izotopes", "Rare Izotopes"};


void MassFitter::on_pbtnGenerate_clicked()
{
	g_bSkipIzotopes = ui.rbtnSkipIzo->isChecked();
	double MassR=0.0F;
	double MassL=0.0F;
	double HW=0.0F;

	double fWin=0.001F;

//	int BFCount=0;

	//float BFMass[1000];
	//float MassDif[1000];
	//int SortInd[1000];
	//char* BFList[1000];
	//for(int i=0;i<1000;i++) 
	//{
	//	BFList[i]=new char[100];
	//}


	
	double fWidth=0.0F;

//	GCnt=new int[150]; 
//	GInd=new int[150]; 

//	char sLine[150];

	//char sPart1[15000];
	//char sPart2[15000];
//	char szOutLine[1000];
/*
	//getting data from input
	QString sInput = ui.InputTextEdit->toPlainText();
	QTextStream istream(&sInput);
	QString qsLine;
	nPeakCount=0;
	while (!istream.atEnd()){
		qsLine = istream.readLine();
		QTextStream linestream(&qsLine);
		linestream>>PeakM[nPeakCount]>>PeakW[nPeakCount];
//		PeakM[nPeakCount] += g_fElMass;
		nPeakCount++;				
	}
*/	
	g_MainBF.clear();
	g_RareBF.clear();
//	g_BrutoList.clear();
	g_nBranches = 0;

	double fM = ui.doubleSpinBox_Mass->value();
	double fW = ui.doubleSpinBox_dM->value();

	//Generating
	/*
	for(int i1=0;i1<nPeakCount;i1++)
	{
	*/
		
		double f_dM=0.5F*fW;
#ifdef 	MAXERR	
		f_dM=MAXERR*0.5;
#endif
		MassR=fM+f_dM;
		MassL=fM-f_dM;
		
//		sprintf(szOutLine,"---------------------------------------------------------------");
		ui.OutTextEdit->append("---------------------------------------------------------------");
//		sprintf(szOutLine,"Peak MZ=%f",PeakM[i1]);
		ui.OutTextEdit->append(QString("Peak MZ=%1").arg(QString::number(fM)));
//		ui.OutTextEdit->append("");
	
//		BFCount=0;
		g_BrutoList.clear();
		DWORD dwTime = GetTickCount();
		//-------------------------------------------------------------------------------------------
		NBrutoFormula testBF;
		split(&testBF,0,MassL,MassR);
		//-------------------------------------------------------------------------------------------
		dwTime = GetTickCount() - dwTime;
		ui.OutTextEdit->append(QString("Calculation time is %1 ms").arg(QString::number(dwTime)));
		int nBFlength = g_BrutoList.size();

		QList<NBrutoFormula>::iterator itBF;// = g_BrutoList.begin();
		
		for(itBF = g_BrutoList.begin();itBF != g_BrutoList.end(); itBF++){
			QString sIzForm;
			std::list<NBFItem>::iterator itBFit = itBF->m_Items.begin();
			NBrutoFormula chemBF;
			for(;itBFit != itBF->m_Items.end();itBFit++){
				if (itBFit->m_nIndex >1){
					sIzForm += QString("%1(%2)%3").arg(itBFit->m_pIzotop->m_pEl->m_sName).arg(QString::number(itBFit->m_pIzotop->m_nMass)).arg(QString::number(itBFit->m_nIndex));
				}else{
					sIzForm += QString("%1(%2)").arg(itBFit->m_pIzotop->m_pEl->m_sName).arg(QString::number(itBFit->m_pIzotop->m_nMass));
				}
				bool bFound = false;
				for(std::list<NBFItem>::iterator itChBF = chemBF.m_Items.begin();itChBF != chemBF.m_Items.end() && !bFound;itChBF++){
					if (itChBF->m_pIzotop->m_pEl == itBFit->m_pIzotop->m_pEl){
						bFound = true;
						itChBF->m_nIndex += itBFit->m_nIndex;
					}
				}
				if (!bFound){
					chemBF.AddItem(*itBFit);
				}
			}
			QString sChemForm;
			for(itBFit = chemBF.m_Items.begin();itBFit != chemBF.m_Items.end();itBFit++){
				if (itBFit->m_nIndex >1){
					sChemForm += QString("%1%2").arg(itBFit->m_pIzotop->m_pEl->m_sName).arg(QString::number(itBFit->m_nIndex));
				}else{
					sChemForm += QString("%1").arg(itBFit->m_pIzotop->m_pEl->m_sName);
				}
			}
			itBF->m_fDM = fM - itBF->m_fMass;
			itBF->m_fPPM = itBF->m_fDM*1E6/itBF->m_fMass;
			itBF->m_sChemForm = sChemForm;
			itBF->m_sIzotForm = sIzForm;

			switch(itBF->m_nType)
			{
			case 0:
				g_MainBF.push_back(*itBF);
				break;
			case 1:
				g_RareBF.push_back(*itBF);
				break;
			}

					
			//QString sLine;
			//float fM = itBF->m_fMass;
			//sLine.sprintf("M=%f\tdM=%f\t",fM,PeakM[i1]-fM);
			//sLine += sChemForm + "\t" + sIzForm;
			//ui.OutTextEdit->append(sLine);
		}

		ui.OutTextEdit->append(QString("Main Isotopes %1; Rare Izotopes %2\n").arg(QString::number(g_MainBF.size())).arg(QString::number(g_RareBF.size())));

		int nMainBFSize = g_MainBF.size();
		if (nMainBFSize){
			g_nBranches = 1;
			for(int i=0;i<nMainBFSize;i++){
				for(int j=i+1;j<nMainBFSize;j++){
					if (fabs(g_MainBF[i].m_fDM) > fabs(g_MainBF[j].m_fDM)){
						g_MainBF.swap(i,j);
					}
				}
			}
		}
		int nRareBFSize = g_RareBF.size();
		if (nRareBFSize){
			g_nBranches = 2;
			for(int i=0;i<nRareBFSize;i++){
				for(int j=i+1;j<nRareBFSize;j++){
					if (fabs(g_RareBF[i].m_fDM) > fabs(g_RareBF[j].m_fDM)){
						g_RareBF.swap(i,j);
					}
				}
			}
		}
		g_IzModel.Update();
//	}

	ui.treeView->expand(g_IzModel.index(0,0));
	if (g_nBranches = 2){
		ui.treeView->expand(g_IzModel.index(1,0));
	}
	setFocusProxy(ui.treeView);
//	g_stdmodel.clear();
	//QModelIndex indMain = g_stdmodel.index(0, 0);
	//g_stdmodel.setData(indMain, QString("Main Izotopes(%1)").arg(QString::number(nMainSize)));
	//g_stdmodel.insertRows(0,nMainSize,indMain);
	//g_stdmodel.insertColumns(0,4,indMain);

	//g_stdmodel.setHorizontalHeaderLabels (QStringList()<<"Chem Formula"<<"Izotop Formula"<<"Mass"<<"dM");
	//QList<NBrutoFormula>::iterator itBF = g_MainBF.begin();
	//for (int nRow = 0; nRow < nMainSize; ++nRow,itBF++) {
	//	int nCol = 0;
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indMain), itBF->m_sChemForm);
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indMain), itBF->m_sIzotForm);
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indMain), QString::number(itBF->m_fMass));
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indMain), QString::number(itBF->m_fDM));
	//}
	//QModelIndex indRare = g_stdmodel.index(1, 0);
	//g_stdmodel.setData(indRare, QString("Rare Izotopes(%1)").arg(QString::number(nRareSize)));
	//g_stdmodel.insertRows(0,nRareSize,indRare);
	//g_stdmodel.insertColumns(0,4,indRare);
	//itBF = g_RareBF.begin();
	//for (int nRow = 0; nRow < nRareSize; ++nRow,itBF++) {
	//	int nCol = 0;
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indRare), itBF->m_sChemForm);
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indRare), itBF->m_sIzotForm);
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indRare), QString::number(itBF->m_fMass));
	//	g_stdmodel.setData(g_stdmodel.index(nRow, nCol++, indRare), QString::number(itBF->m_fDM));
	//}
	//ui.treeView->setModel(&g_stdmodel);


	//int nGoodWidth = ui.OutTextEdit->minimumSizeHint().width();
	//ui.OutTextEdit->setMinimumWidth(nGoodWidth);
}

void MassFitter::openDataFile(const QString& sFName)
{
	/*
	QFile dataFile(sFName);	
	//Reading of peak list
	if (dataFile.open(QIODevice::ReadOnly)){
		QTextStream istream(&dataFile);
		QString sLine;
		while (!istream.atEnd()){
			sLine = istream.readLine();
			ui.InputTextEdit->append(sLine);
		}
		ui.OutTextEdit->append("Data Loaded");
		dataFile.close();
	}
	*/
}
void MassFitter::openChemFile(const QString& sFName)
{

//	ElemCount=0;

	QFile chemFile(sFName);	
	if (chemFile.open(QIODevice::ReadOnly)){
		g_IzList.clear();
		g_ElList.clear();
		QTextStream istream(&chemFile);
		QString sLine = istream.readLine();
//		ElemCount = sLine.toInt();
		int i =0;
		QString sName;
		int nMass;
		double fMass;
		int nMaxCount;
		int nGrp;
		int nType;
		while (!istream.atEnd()){
			sLine = istream.readLine();
			QTextStream sStream(&sLine);
//			sStream>>MInt[i]>>E[i]>>M[i]>>MaxC[i]>>G[i]>>T[i];
//			i++;
			sStream>>nMass>>sName>>fMass>>nMaxCount>>nGrp>>nType;
			g_IzList.push_back(NIzotop(g_ElList, sName,fMass,nMass,nMaxCount,nGrp,nType));
		}
		int IzCount = g_IzList.size();
		int ElCount = g_ElList.size();
		ui.OutTextEdit->append("Chem Loaded. " + QString::number(IzCount) + " Izotopes. " + QString::number(ElCount) + " Elements found.");		
		chemFile.close();
		g_ElModel.Update();
	}else{
		ui.OutTextEdit->append("Error Loading Chem");
	}
}

void MassFitter::SetMassForFit(double fMass, double fdMass)
{
	ui.doubleSpinBox_Mass->setValue( fMass );
	ui.doubleSpinBox_dM->setValue( fdMass );
	/*
	ui.InputTextEdit->clear();
//	QString sLine;
	ui.InputTextEdit->append( QString::number(fMass) + " " + QString::number(fdMass));
//	on_pbtnGenerate_clicked();
	*/
}

 QAXFACTORY_BEGIN("{38824414-535B-419F-B9D6-C6748C20FCF3}", "{0316A9C2-A3F6-4FC9-A1F4-45EDEE0C4D45}")
     QAXCLASS(MassFitter)
 QAXFACTORY_END()
/*
QAXFACTORY_DEFAULT(MassFitter,
	   "{38824414-535B-419F-B9D6-C6748C20FCF3}",
	   "{0316A9C2-A3F6-4FC9-A1F4-45EDEE0C4D45}",
	   "{435E1A9C-4128-42E0-A41F-AD7BB3453D12}",
	   "{A8F97B80-CEC6-42D5-B87D-0461D5A416D6}",
	   "{1FC3E38B-2399-46DD-A091-AA0E48545E09}")
*/



void MassFitter::on_btnSavePreset_clicked()
{
	QString sFileName = QFileDialog::getSaveFileName(NULL,"Save Chemestry Preset File",sDefDir,"*.pch");
	QFile setFile(sFileName);	
	if (setFile.open(QIODevice::WriteOnly)){
		QTextStream stream(&setFile);
		QList<NElement>::iterator itEl = g_ElList.begin();
		for(;itEl != g_ElList.end();itEl++){
			stream<<QString("%1\t%2\r\n").arg(itEl->m_sName).arg(QString::number(itEl->m_nMaxCount));
		}
		setFile.close();
	}
}

 void MassFitter::openPresetFile(const QString& sFileName)
 {
	QFile setFile(sFileName);	
	if (setFile.open(QIODevice::ReadOnly)){
		QTextStream stream(&setFile);
		QList<NElement>::iterator itEl = g_ElList.begin();
		for(;itEl!=g_ElList.end();itEl++){
			itEl->m_nMaxCount = 0;
		}
		QString sName;
		int nMaxCount;
		while (!stream.atEnd()){
			stream>>sName>>nMaxCount;
			bool bFound = false;
			for(itEl = g_ElList.begin(); itEl != g_ElList.end() && !bFound; itEl++){
				if (sName == itEl->m_sName){
					bFound = true;
					itEl->m_nMaxCount = nMaxCount;
				}
			}
		}
		
		setFile.close();
		g_ElModel.Update();
	}
 }
void MassFitter::on_btnLoadPreSet_clicked()
{
	QString sFileName = QFileDialog::getOpenFileName(NULL,"Open Chemestry Preset File",sDefDir,"*.pch");
	openPresetFile(sFileName);
}

void MassFitter::on_btnLoadChem_clicked()
{
	QString sFileName = QFileDialog::getOpenFileName(0,"Open chemestry data file",sDefDir,"*.chmr");
	openChemFile(sFileName);
	g_ElModel.Update();
}
void MassFitter::selTreeChanged ( const QItemSelection & selected, const QItemSelection & deselected )
{
	if (selected.indexes().size()){
		const QModelIndex& ind = selected.indexes().first();
		if (ind.parent().isValid()){
			QString sChemForm = g_IzModel.data(g_IzModel.index(ind.row(),0,ind.parent())).toString();
		}
	}
}
void MassFitter::TreeCurrentChanged ( const QModelIndex & current, const QModelIndex & previous )
{
	if (current.parent().isValid()){
		QString sChemForm = g_IzModel.data(g_IzModel.index(current.row(),0,current.parent())).toString();
		emit FormulaSelected(sChemForm);
	}
}