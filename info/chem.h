#ifndef _ChemestryStructs_
#define _ChemestryStructs_

#include <QString.h>
#include <QList>

struct NElement
{
	QString m_sName;
	int m_nMaxCount;

	NElement(const QString& sName, int nMaxCount);

	static const double s_fElMass;// = 0.00054858;
};

struct NIzotop
{
	NElement* m_pEl;
	double m_fMass;
	int m_nMass;
	QString m_sName;
	
	int m_nMaxCount;
	int m_nGrp;
	int m_nType;

	NIzotop(QList<NElement>& ElList, const QString& sName, double fMass, int nMass, int nMaxCount, int nGrp, int nType);
};
struct NBFItem
{
	const NIzotop* m_pIzotop;
	int m_nIndex;
	NBFItem(const NIzotop* pIz, int nIndex);
	NBFItem(const NBFItem& bfIt);
};

struct NBrutoFormula
{
	std::list<NBFItem> m_Items;
	double m_fMass;
	int m_nType;
	double m_fDM;
	double m_fPPM;
	QString m_sChemForm;
	QString m_sIzotForm;

	NBrutoFormula();// : 	m_fMass( 0.0 ) , m_nType(0) /*, m_sText("")*/ {	}
	NBrutoFormula(const NBrutoFormula& baseBF);// : 	m_fMass( 0.0 ) , m_nType(0) /*, m_sText("")*/ {	}
	void AddItem(const NBFItem& bfIt);
	
	//NBrutoFormula(const NBrutoFormula& bf)
	//	: m_Items(bf.m_Items), m_fMass(bf.m_fMass), m_nType(bf.m_nType)		{	}
	
};

#endif