#include "chem.h"

const double NElement::s_fElMass = 0.00054858;

NElement::NElement(const QString& sName, int nMaxCount)
		: m_sName(sName), m_nMaxCount(nMaxCount)	
{
}

NIzotop::NIzotop(QList<NElement>& ElList, const QString& sName, double fMass, int nMass, int nMaxCount, int nGrp, int nType)
		: m_fMass(fMass) , m_nMass(nMass), m_nMaxCount(nMaxCount), m_nGrp(nGrp), m_nType(nType)
{
	QList<NElement>::iterator itEl = ElList.begin();
	bool bFound = false;
	for(;itEl != ElList.end() && !bFound; itEl++){
		bFound = (sName == itEl->m_sName);
	}
	if (bFound){
		itEl--;
		itEl->m_nMaxCount += nMaxCount;
		m_pEl = &(*itEl);
	}else{
		ElList.push_back(NElement(sName,nMaxCount));
		m_pEl = &ElList.last();
	}
	if (m_nType){
		m_sName = QString::number(nMass)+sName;
	}else{
		m_sName = sName;
	}
}

NBFItem::NBFItem(const NIzotop* pIz, int nIndex)
{
	m_pIzotop = pIz;
	m_nIndex = nIndex;
}
NBFItem::NBFItem(const NBFItem& bfIt)
{
	m_pIzotop = bfIt.m_pIzotop;
	m_nIndex = bfIt.m_nIndex;
}
NBrutoFormula::NBrutoFormula() 
{
	m_nType = 0;

	m_fMass = -NElement::s_fElMass;
	m_fDM = 0;
	m_fPPM = 0;
	m_sChemForm = "";
	m_sIzotForm = "";
}
NBrutoFormula::NBrutoFormula(const NBrutoFormula& baseBF)
	: m_Items(baseBF.m_Items)
{
	m_fMass = baseBF.m_fMass;
	m_nType = baseBF.m_nType;
	m_fDM = baseBF.m_fDM;
	m_fPPM = baseBF.m_fPPM;
	m_sChemForm = baseBF.m_sChemForm;
	m_sIzotForm = baseBF.m_sIzotForm;
}
void NBrutoFormula::AddItem(const NBFItem& bfIt)
{
	m_Items.push_back(bfIt);
	m_fMass += bfIt.m_nIndex*bfIt.m_pIzotop->m_fMass;
	int nIzType =  bfIt.m_pIzotop->m_nType;
	if (m_nType == 0 && nIzType != 0) m_nType = nIzType;
}

