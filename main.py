#!/usr/bin/env python

import wsgiref.handlers
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.runtime import DeadlineExceededError

import splitmass
from splitmass import Izotope
from splitmass import splitmass

from initIzotopes import initIzotopes

class MyHandler(webapp.RequestHandler) :
	liIzotopes = []

	def __init__(self):
		webapp.RequestHandler.__init__(self)
		self.liIzotopes = initIzotopes()
	
	def pong(self):
		self.response.set_status(202)
	def get(self):
		ElList = []
		for iz in self.liIzotopes:
			if not (iz.signEl in ElList):
				ElList.append( iz.signEl )
		#";".join(["%s=%s" % (k, v) for k, v in params.items()])
		#sElList = ";".join(["%s" % (s,) for s,i in ElList.items()])
		sElList = ";".join(ElList)
		params = {'defElPattern':'C_20;H_50;N_10;O_10','elemList':sElList}
		self.response.out.write(template.render("main.html",params))
	def post(self):
		massVal = float(self.request.get('massVal'))
		massWin = float(self.request.get('massWin'))
		skipRare = bool(self.request.get('skipRare'))
		sElPattern = self.request.get('palElem')
		
		diEl = {}
		for sPair in sElPattern.split(";"):
			(sign,cnt) = sPair.split("_")
			diEl[sign] = int(cnt)
			
		locIz = []
		for iz in self.liIzotopes:
			if (iz.signEl in diEl):
				locIz.append(iz)
		
		#self.response.out.write(diEl)
		liBF = []
		#sRes = ""
		#for iz in self.liIzotopes:
		#	sRes += "_%s" % (iz.sign,)
		#self.response.out.write("Processing request...")
		try:
			splitmass(diEl,locIz,skipRare,None,0\
			,massVal - massWin/2,massVal + massWin/2,liBF,self)
		except DeadlineExceededError:
			self.response.clear()
			self.response.set_status(500)
			self.response.out.write("This operation could not be completed in time...")


		sBFRes =""
		for BF in liBF:
			sBFRes += "<p>%s ; massErr=%f</p>" % (BF.toStr(),BF.ionmass - massVal )

		sSkip = ''
		if skipRare:
			sSkip = '(skiping Rare)'
		#self.response.clear()
		self.response.out.write("<h2>possible formula for: %f in win %f %s is:</h2>%s" % (massVal,massWin,sSkip,sBFRes))

def main():
	#print "Hello"
	app = webapp.WSGIApplication([(r'.*',MyHandler)],debug=True)
	wsgiref.handlers.CGIHandler().run(app)

if __name__ == "__main__":
	main()