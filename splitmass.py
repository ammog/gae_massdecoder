class Izotope:
	""" sign, real mass, nuclons count, abundance, and izotope type (common or rare)"""
	signEl = 'X'
	signIz = 'X'
	mass = 0.0
	nucl = 0
	abun = 0.0
	rare = 0
	
	def __init__(self,_sign,_mass, _abun,_rare=0):
		self.signEl = _sign
		_nucl = int(round(_mass))
		if _rare==0:
			self.signIz = _sign
		else:
			self.signIz = "%s(%d)" % (_sign,_nucl)
		self.mass = _mass
		self.nucl = _nucl
		self.abun = _abun
		self.rare = _rare

electronmass = 0.00054858;
class BrutoFormula:
	"""  describes what Izotopes with what index are included in formula"""
	
	neutralmass = 0.0
	ionmass = -neutralmass - electronmass;
	elements = []
	elcount = {}
		
	def __init__(self):
		#self.neutralmass = 0.0;
		self.ionmass = - electronmass
		self.elements = []
		self.elcount = {}
		
	def copy(self, _BF):
		#self.neutralmass = _BF.neutralmass;
		self.ionmass = _BF.ionmass;
		self.elements = _BF.elements[:]
		self.elcount = _BF.elcount.copy()
	 
	def addElement(self,Izot,index):
		#print "add %s%d: %f to %s" % (Izot.signIz,index,Izot.mass*index,self.toStr())
		#print self.elcount
		self.elements.append((Izot,index))
		#self.neutralmass += Izot.mass*index;
		self.ionmass += Izot.mass*index
		if Izot.signEl in self.elcount:
			self.elcount[Izot.signEl] = self.elcount[Izot.signEl] + 1
		else:
			self.elcount[Izot.signEl] = 1
		#print self.elcount

	 
	def toStr(self):
		str = ""
		for iz,index in self.elements:
			iz_sign = iz.signIz
			if (index == 1):
				str += iz_sign
			else:
				str += "%s%d" % (iz_sign,index)
			
		#str = "".join(["%s%d" % (Iz.sign, Ind) for Iz, Ind in self.elements])
		return "%s: ionmass=%f" % (str, self.ionmass)

def splitmass(diEl,liIz,skipRare,baseBF,fromIzNum,massmin,massmax,liBF,stateHandle=None):
	if baseBF == None:
		baseBF = BrutoFormula()
	#print "baseBF: %s ; fromIz: %d" % (baseBF.toStr(),fromIzNum)
	for IzNum in range(fromIzNum,len(liIz)):#liIzotopes[fromIzNum:]
		#print Iz.sign
		#(Iz_sign,Iz_mass,Iz_abund,Iz_type) = Iz
		Iz = liIz[IzNum]
		#print "%d:%s" % (IzNum,Iz.signIz)
		maxIndex = int((massmax-baseBF.ionmass) / Iz.mass)
		if skipRare:
			if Iz.rare != 0:
				maxIndex = 0
		El_maxI = diEl[Iz.signEl]
		BF_ElCount = 0
		if Iz.signEl in baseBF.elcount:
			BF_ElCount = baseBF.elcount[Iz.signEl]
 		#print "El MaxI %d; BF El count %d; MaxI = %d maxIndex=%d" % (El_maxI,BF_ElCount,El_maxI-BF_ElCount,maxIndex)
		El_maxI -= BF_ElCount
			
		if maxIndex > El_maxI:
			maxIndex = El_maxI
		#print "final maxIndex = %d" % (maxIndex,)
		for i in range(maxIndex):
			ind = i + 1
			newmass = baseBF.ionmass + Iz.mass*ind;
			
			if (newmass < massmax):
				newBF = BrutoFormula()
				newBF.copy(baseBF)
				newBF.addElement(Iz,ind)
				#print "newBF: %s" % (newBF.toStr(),)
				if (newmass > massmin):
					liBF.append(newBF)
					#print "     found BF: %s" % (newBF.toStr(),)
				else:
					splitmass(diEl,liIz,skipRare,newBF\
						,IzNum+1,massmin,massmax,liBF)
			else:
				if stateHandle != None:
					stateHandle.pong()
	
def massfit(massVal,massWin):
	print "massVal=%f\tmassWin=%f" % (massVal,massWin)
	diEl = {'C':2,'H':10,'O':1,'N':1}
	#Izotopes = [('C',12,0.9,True)#,('C',13,0.1,False)\
	#,('H',1,1,True),('O',18,1,True)]
	
	liIzotopes =[]
	liIzotopes.append(Izotope('C', 12.00000000, 100, 0))
#	liIzotopes.append(Izotope('C', 13.00335484,   1, 1))
	liIzotopes.append(Izotope('H', 1.007825032,   1, 0))
#	liIzotopes.append(Izotope('N', 14.00307401,  10, 0))
#	liIzotopes.append(Izotope('N', 15.00010890,  10, 1))
	liIzotopes.append(Izotope('O', 15.99491462,  10, 0))
#	liIzotopes.append(Izotope('O', 17.99916040,  10, 1))
	
	liBF = []
	#baseBF = BrutoFormula()
	
	splitmass(diEl,liIzotopes,False,None,0\
		,massVal - massWin/2,massVal + massWin/2,liBF)
	print "%s\n%s" % ("Found Formula:","\n".join("%s" % (BF.toStr(),) for BF in liBF) )

#massfit(46.041865,0.01)